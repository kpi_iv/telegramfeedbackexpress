build:
	cd server && npm run build && cd ../client && npm run build

start:
	cd server && npm start

dev:
	cd server && npm run dev