import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ApiService } from './api.service'
import { MessagesService } from './messages.service'
import { ChatService } from './chat.service'

import { Chat } from './models/chat';

const CHATS_KEY: string = 'local_chats';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  allowInternetConnection: Boolean;
  
  defaultSettings: Object = {
    allowInternetConnection: true,
    selectedChat: undefined
  }

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService, private apiService: ApiService) {
      const settings = this.getSettings()
      this.allowInternetConnection = settings.allowInternetConnection
  }

  /**
   * 
   *  Initial message Loading
   * 
  */
  public setLocalStorageChats(){
    return new Promise((res, rej)=>{
      this.apiService.getChats()
        .subscribe((data) =>{
          this.storage.set(CHATS_KEY, data);
          res(data)
        },(err)=>{
          console.log(err); 
          res()
        });
    })
  }

  public setLocalStorageMessages(id){
    return new Promise((res, rej)=> {
      if(this.allowInternetConnection){
        this.apiService.getMsgByChatId(id)
        .subscribe((data) => {
          this.storage.set(`message_for_chat_${id}`, data);
          res(data)
        },(err)=>{
          console.log(err);
          res()
        });
      }
      else{
       res(this.storage.get(`message_for_chat_${id}`)||[]); 
      }
    })
  }

  /**
   * 
   *  Local storage modifications
   * 
  */
  public setSettings(key, value){
    const settings = this.getSettings()
    settings[key] = value
    this.storage.set('settings', settings)
  }

  public setLocalStorageMessagesNew(message, chatId): void {
    const chats = this.storage.get(CHATS_KEY);
    const messageStorage =  this.storage.get(`message_for_chat_${chatId}`)
    messageStorage.push(message)
    this.storage.set(`message_for_chat_${chatId}`, messageStorage);
  }

  public pushLocalMessageToDump(message, chatId){
    const dump = this.getLocalMessagesDump()
    dump.push({message, chatId})
    this.storage.set(`local_dump`, dump);
  }

  public clearLocalMessageDump(){
    this.storage.set(`local_dump`, {});
  }


  /**
   *  
   * Get Local storage Values
   * 
  */

  public getLocalMessagesDump(){
    return this.storage.get(`local_dump`)||[]
  }

  public getSettings(){
    const settings = this.storage.get(`settings`)
    if(!settings){
      this.storage.set(`settings`, this.defaultSettings);
      return this.defaultSettings
    }
    return settings
  }

  public getLocalStorageMessages(id: number): void {
    return this.storage.get(`message_for_chat_${id}`);
  }

  public getLocalStorageChats(){
    return this.storage.get(CHATS_KEY) || [];
  }

  public getLocalStorageChatsIds(){
    const chats =  this.storage.get(CHATS_KEY) || [];
    return chats.map(i=>i.id)
  }
}
