import {
    trigger,
    state,
    style,
    animate,
    transition,
    sequence,
    query,
    group,
    keyframes
} from '@angular/animations';

//.overlay, 
export default () => {
    return [
        trigger('openClose', [
            state('open', style({
                left: '0px',
            })),
            state('closed', style({
                left: '*',
            })),
            transition('closed => open', [
                animate('0.2s', style({ left: '0px' }))
            ]),
            transition('open => closed', [
                animate('0.2s', style({ left: '*' }))
            ])
        ])
    ]
}

export const overlay = () => {
    return [
        trigger('openClose', [
            state('open', style({
                display: 'block',
                opacity: 1
            })),
            state('closed', style({
                display: 'none',
                opacity: 0
            })),
            transition('closed => open', [
                style({ display: 'block' }),
                animate('0.2s', keyframes([
                    style({ opacity: 0, offset: 0 }),
                    style({ opacity: 1, offset: 1 }),
                ]))
            ]),
            transition('open => closed', [
                animate('0.2s', keyframes([
                    style({ opacity: 1, offset: 0 }),
                    style({ opacity: 0, offset: 1 }),
                ]))
            ])
        ]),
        trigger('openClose2', [
            state('open', style({
                display: 'block',
            })),
            state('closed', style({
                display: 'none',
            })),
            transition('* <=> *', [
                animate('0.2s', style({ display: '*' }))
            ])
        ])

    ]
}


export const config = () => {
    return [
        trigger('openClose', [
            state('open', style({
                display: 'block'
            })),
            state('closed', style({
                display: 'none'
            })),
            transition('closed => open', [
                animate('0.2s', style({ display: 'block' }))
            ]),
            transition('open => closed', [
                animate('0.2s', style({ display: 'none' }))
            ])
        ])
    ]
}