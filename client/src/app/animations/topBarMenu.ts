import {
    trigger,
    state,
    style,
    animate,
    transition,
    sequence,
    query,
    group,
    keyframes
} from '@angular/animations';

export default () => {
    return [
        trigger('openClose', [
            state('open', style({
                'padding-top': '*',
                'padding-bottom': '*',
                display: '*'
            })),
            state('closed', style({
                'padding-top': 0,
                'padding-bottom': 0,
                display: 'none'
            })),
            transition('open => closed', [
                sequence([
                    query('.option', [
                        group([
                            animate('0.25s', keyframes([
                                style({ height: '50px', offset: 0 }),
                                style({ height: '0px', offset: 1 }),
                            ])),
                            animate('0.25s', keyframes([
                                style({ opacity: 1, offset: 0 }),
                                style({ opacity: 0, offset: 0.5 }),
                            ])),
                        ])
                    ]),
                    query(':self', [
                        animate('0.05s', style({ 'padding-top': 0, 'padding-bottom': 0 })),
                    ]),
                ])
            ]),
            transition('closed => open', [
                style({ display: '*' }),
                query('.option', [
                    style({ opacity: 0 }),
                    style({ height: 0 }),
                ]),
                sequence([
                    query(':self', [
                        animate('0.05s', style({ 'padding-top': '*', 'padding-bottom': '*' })),
                    ]),
                    query('.option', [
                        group([
                            animate('0.25s', keyframes([
                                style({ height: '0px', offset: 0 }),
                                style({ height: '50px', offset: 1 }),
                            ])),
                            animate('0.25s', keyframes([
                                style({ opacity: 0, offset: 0.5 }),
                                // style({ opacity: 0.8, offset: 0.5 }),
                                style({ opacity: 1, offset: 1 }),
                            ])),
                        ])
                    ]),
                ])
            ]),
        ]),
        trigger('openCloseChildren', [
            state('open', style({
                margin: '*',
                opacity: '*',
                height: '*'
            })),
            state('closed', style({
                margin: 0,
                opacity: 0,
                height: 0
            })),
        ]),
    ]
}