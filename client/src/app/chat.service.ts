import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MessagesService } from './messages.service'
import { LocalStorageService } from './local-storage.service'
import { _ } from 'underscore';

import { Chat } from './models/chat';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  chats: Chat[];
  selectedChat: Chat;

  constructor(private messegesService: MessagesService, private localStorageService: LocalStorageService) {}

  async setChat(){
    const chatObject = await this.localStorageService.setLocalStorageChats()
    this.chats = Object.values(chatObject).map(chat => {
      return new Chat(chat)
    })
  }

  setSelectedChat(chat: Chat) {
    if(this.selectedChat != chat){
      this.selectedChat = chat
      this.messegesService.setMsgs(chat.id)
      this.localStorageService.setSettings('selectedChatid', chat.id)
    }
  }

  getChats(): Observable<Chat[]> {
    return of(this.chats)
  }
}
