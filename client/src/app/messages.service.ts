import { Injectable, EventEmitter } from '@angular/core';

import { Msg, MsgType, ReadStatus } from './models/msg'

import { LocalStorageService } from './local-storage.service'

import { ApiService } from './api.service'

// RxJS
// import {Observable, Observer} from 'rxjs';
// import 'rxjs/add/operator/share';

@Injectable({
  providedIn: 'root'
})

export class MessagesService {
  msgs: Msg[];
  msgsHolder: Object = {};
  // scrollDownTrigger = new EventEmitter<Object>();

  constructor(private localStorageService: LocalStorageService, private apiService: ApiService) {}

  async setMsgs(chatId: number) {
    
    this.msgsHolder[chatId] = await this.localStorageService.setLocalStorageMessages(chatId);
    this.msgs = this.msgsHolder[chatId]
    // this.scrollDownTrigger.emit(0)
    
  }

  sendMsgs(messageText: String, chatId: number){
    const msgsHolder_ = this.msgsHolder[chatId]
    if(!msgsHolder_) return

    const newId = msgsHolder_[msgsHolder_.length-1].id
    const newMessage = {
      id: newId,
      message: messageText,
      timeout: "00:00",
      type: MsgType.Send,
      readStatus: ReadStatus.done
    }

    msgsHolder_.push(newMessage)
    this.msgs = msgsHolder_
    this.localStorageService.setLocalStorageMessagesNew(newMessage, chatId);

    this.pushOrDumpNewMessage(newMessage, chatId)
    // this.scrollDownTrigger.emit(0)
  }

  pushOrDumpNewMessage(newMessage, chatId){
    if(this.localStorageService.allowInternetConnection){
      this.apiService.pushNewMessage(newMessage, chatId).subscribe(data=>{})
    }
    else {
      this.localStorageService.pushLocalMessageToDump(newMessage, chatId)
    }
  }

  internetChanged(){
    const allowInternetConnection = !this.localStorageService.allowInternetConnection
    this.localStorageService.setSettings('allowInternetConnection', allowInternetConnection)

    if(allowInternetConnection)
    {
      const dump = this.localStorageService.getLocalMessagesDump()
      if(dump.length>0)
      {
        dump.forEach(element => {
          this.apiService.pushNewMessage(element.message, element.chatId).subscribe(data=>{})
        });
        this.localStorageService.clearLocalMessageDump()
      }
    }
  }
}
