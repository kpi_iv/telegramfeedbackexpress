import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { MessagesService } from '../messages.service';

import { Msg } from '../models/msg'

@Component({
  selector: 'app-conv-history',
  templateUrl: './conv-history.component.html',
  styleUrls: ['./conv-history.component.scss']
})
export class ConvHistoryComponent implements OnInit {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  private _eventSubscription;

  constructor(public messagesService: MessagesService) { }

  ngAfterViewChecked() {        
    this.scrollToBottom();        
  } 

  scrollToBottom(): void {
    try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }                 
  }

  ngOnInit() {
    // this.messagesService.scrollDownTrigger.subscribe(
    //   (value) => {
    //     // this.scrollToBottom()
    //   },
    //   (error) => console.error(error)
    // );
  }

  ngOnDestroy() {
    // this._eventSubscription.unsubscribe();
  }
}
