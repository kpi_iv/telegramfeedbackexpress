import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvHistoryComponent } from './conv-history.component';

describe('ConvHistoryComponent', () => {
  let component: ConvHistoryComponent;
  let fixture: ComponentFixture<ConvHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
