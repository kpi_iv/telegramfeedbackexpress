import { Component, OnInit, HostListener } from '@angular/core';

import animation from '../animations/topBarMenu'

@Component({
  selector: 'app-top-bar',
  animations: animation(),
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  isOpen = false;
  constructor() { }

  @HostListener('mouseleave')
  onMouseLeave() {
    setTimeout(() => this.toggleOff(), 100)
  }
  ngOnInit() {
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }
  toggleOn() {
    this.isOpen = true;
  }
  toggleOff() {
    this.isOpen = false;
  }

}
