import { Component, OnInit, Input } from '@angular/core';

import { MessagesService } from '../messages.service'
import { ChatService } from '../chat.service'

@Component({
  selector: 'app-reply-bar',
  templateUrl: './reply-bar.component.html',
  styleUrls: ['./reply-bar.component.scss']
})
export class ReplyBarComponent implements OnInit {

  @Input() messageText: String = ''; 

  constructor(private messagesService: MessagesService, private chatService: ChatService) { }

  onKeydown(event) {
    if(event.key === "Enter"){
      const messageText = this.messageText
      this.messageText = '';
      this.messagesService.sendMsgs(messageText, this.chatService.selectedChat.id);
      
    }
  }
  ngOnInit() {
  }

}
