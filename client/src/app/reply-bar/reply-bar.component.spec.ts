import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReplyBarComponent } from './reply-bar.component';

describe('ReplyBarComponent', () => {
  let component: ReplyBarComponent;
  let fixture: ComponentFixture<ReplyBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReplyBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReplyBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
