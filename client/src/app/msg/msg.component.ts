import { Component, OnInit, Input } from '@angular/core';
import { Msg, MsgType, ReadStatus } from '../models/msg';

@Component({
  selector: 'app-msg',
  templateUrl: './msg.component.html',
  styleUrls: ['./msg.component.scss']
})
export class MsgComponent implements OnInit {

  @Input() msg: Msg;
  MsgType: typeof MsgType = MsgType;
  ReadStatus: typeof ReadStatus = ReadStatus
  constructor() { }

  ngOnInit() {
  }

}
