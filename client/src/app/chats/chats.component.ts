import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';

import { Chat } from '../models/chat';
import { LocalStorageService } from '../local-storage.service'

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit {
  constructor(public chatService: ChatService, private localStorageService: LocalStorageService) { }

  async ngOnInit() {
    await this.chatService.setChat()
    const savedSelectedChatID = this.localStorageService.getSettings().selectedChatid
    if(savedSelectedChatID && savedSelectedChatID < this.chatService.chats.length){
      this.chatService.setSelectedChat(this.chatService.chats[savedSelectedChatID])
    } else if (this.chatService.chats && this.chatService.chats.length > 0) {
      this.chatService.setSelectedChat(this.chatService.chats[0])
      this.localStorageService.setSettings('selectedChatid', this.chatService.chats[0].id)
    }
  }

  onSelect(chat: Chat): void {
    this.chatService.setSelectedChat(chat)
  }

}
