import { Component, OnInit } from '@angular/core';
import { config } from '../animations/leftMenu'
import { MenuService } from '../menu.service'
import { LocalStorageService } from '../local-storage.service'
import { MessagesService } from '../messages.service'

@Component({
  selector: 'app-conf-screen',
  animations: config(),
  templateUrl: './conf-screen.component.html',
  styleUrls: ['./conf-screen.component.scss']
})
export class ConfScreenComponent implements OnInit {

  constructor(private menuService: MenuService, private messagesService: MessagesService, private localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

}
