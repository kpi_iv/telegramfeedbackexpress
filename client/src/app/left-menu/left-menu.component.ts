import { Component, OnInit, HostListener } from '@angular/core';
import { MenuService } from '../menu.service'

import animation from '../animations/leftMenu'

@Component({
  selector: 'app-left-menu',
  animations: animation(),
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {

  constructor(private menuService: MenuService) { }

  ngOnInit() {
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.menuService.triggerESC()
  }

}
