import { Component, OnInit, Input } from '@angular/core';
import { Chat } from '../models/chat';
import { ChatService } from '../chat.service';

@Component({
  selector: 'chat-button',
  templateUrl: './chat-button.component.html',
  styleUrls: ['./chat-button.component.scss']
})

export class ChatButtonComponent implements OnInit {

  @Input() chat: Chat;
  @Input() active: Boolean;
  constructor() { }

  ngOnInit() {
  }

}
