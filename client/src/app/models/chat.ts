export class Chat {
    id: number;
    name: string;
    message: string;
    date: string;
    count: number;
    fixed: Boolean;

    constructor(classObject){
        this.id = classObject.id
        this.name = classObject.name
        this.message = classObject.message
        this.date = classObject.date
        this.count = classObject.count
        this.fixed = classObject.fixed
    }
}