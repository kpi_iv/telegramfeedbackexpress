export class Msg {
    id: number;
    message: string;
    type: MsgType;
    timeout: String;
    readStatus?: ReadStatus;

}

export enum MsgType {
    Received,
    Send
}

export enum ReadStatus {
    sending,
    done,
    read
}