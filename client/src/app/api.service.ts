import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getChats() {
    let URL = "http://localhost:3000/chat";
    return this.http.get(URL);
  }

  getMsgByChatId(id) {
    let URL = `http://localhost:3000/msg/${id}`;
    return this.http.get(URL);
  }

  pushNewMessage(message, chatId) {
    let URL = `http://localhost:3000/push`
    const body = {message, chatId}
    return this.http.post(URL, body);
  }
}
