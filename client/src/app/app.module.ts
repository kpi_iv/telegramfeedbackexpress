import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConvHistoryComponent } from './conv-history/conv-history.component';
import { ReplyBarComponent } from './reply-bar/reply-bar.component';
import { ChatsComponent } from './chats/chats.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { ChatButtonComponent } from './chat-button/chat-button.component';
import { MsgComponent } from './msg/msg.component';
import { LeftMenuComponent } from './left-menu/left-menu.component';
import { ConfScreenComponent } from './conf-screen/conf-screen.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverlayComponent } from './overlay/overlay.component';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    ConvHistoryComponent,
    ReplyBarComponent,
    ChatsComponent,
    TopBarComponent,
    SearchBarComponent,
    ChatButtonComponent,
    MsgComponent,
    LeftMenuComponent,
    ConfScreenComponent,
    OverlayComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    StorageServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
