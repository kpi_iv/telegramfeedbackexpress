import { Component, OnInit } from '@angular/core';
import { MenuService } from '../menu.service'

import { overlay } from '../animations/leftMenu'

@Component({
  selector: 'app-overlay',
  animations: overlay(),
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss']
})
export class OverlayComponent implements OnInit {

  constructor(private menuService: MenuService) { }

  ngOnInit() {
  }

}
