import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {


  isOpen = false;
  isOverlay1 = false;
  isOverlay2 = false;
  isConfig = false;

  constructor() { }

  triggerOn() {
    this.isOpen = true;
    this.isOverlay1 = true;
  }
  triggerOverlay(overlayNum) {
    if (overlayNum == 1)
      this.triggerOff()
    else if (overlayNum == 2)
      this.configOff()
  }
  triggerESC() {
    if (this.isConfig)
      this.configOff()
    else this.triggerOff()
  }
  triggerOff() {
    this.isOpen = false;
    this.isOverlay1 = false;
  }
  configOn() {
    this.isConfig = true;
    this.isOverlay2 = true;
  }
  configOff() {
    this.isConfig = false;
    this.isOverlay2 = false;
  }
}
