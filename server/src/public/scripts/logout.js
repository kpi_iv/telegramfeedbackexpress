const socket = io.connect()
$('#logout').click(() => {
  socket.emit('logout')
  window.location.replace('/private')
})
