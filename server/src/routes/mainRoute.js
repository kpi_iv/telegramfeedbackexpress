'use strict'
import express from 'express'
import { data } from '../routes/socketl'
import * as dbController  from '../db/dbController'

export default (app, passport) => {
    
    app.get('/', isLoggedIn, async (req, res) => {
        console.log(req.session)
        res.sendFile(__dirname.replace(/server\/.*/, 'client/dist/Lab2/index.html'))
    })

    app.get('/logout', function (req, res) {
        req.logout()
        res.redirect('/')
    })

    app.get('/login', function (req, res) {
        if (req.isAuthenticated()) res.redirect('/')
        else res.render('user/login.ejs', { message: req.flash('loginMessage'), Account: req.session.user })
    })

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }))

    app.get('/msg/:id', /*isLoggedIn,*/ async function (req, res) {
        const msg = await dbController.msg.byChatId(req.params.id)
        return res.json(msg)    
    })

    app.get('/chat', /*isLoggedIn,*/ async function (req, res) {
        const chat = await dbController.chat.all()
        return res.status(200).json(chat)
    })

    app.post('/push', /*isLoggedIn,*/ async function (req, res) {
        const newMsg = req.body.message
        newMsg.chatId = req.body.chatId
        await dbController.msg.add(newMsg)
        return res.status(200).json({})
    })
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next()
    res.redirect('/login')
}

export function isLoggedInRessurces(req, res, next) {
    if (req.isAuthenticated()) return next()
    res.status(500)
        .end()
}
