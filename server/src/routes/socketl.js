import fs from 'fs'
import ejs from 'ejs'

import config from '../config/config-file'

export default function (err, socket, session) {

    socket.on('filter', async (obj) => {
        const posts = []

        const file = fs.readFileSync('./src/views/user/table.ejs', 'utf-8')
        const renderedHTML = ejs.render(file, { posts: posts })
        socket.emit('filterBack', { html: renderedHTML, arr: posts })
    })

}
