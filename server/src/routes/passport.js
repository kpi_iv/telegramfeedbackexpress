'use strict'

import { Strategy as LocalStrategy } from 'passport-local'
import config from '../config/config-file'

const AdminUser = { id: 0, name: 'admin' }

export default (passport) => {
    passport.serializeUser(function (user, done) {
        done(null, AdminUser.id)
    })

    passport.deserializeUser(function (id, done) {
        done(null, AdminUser)
    })

    passport.use('local-login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, email, password, done) {
            if (email == config.admin.login && password == config.admin.pass)
                return done(null, AdminUser)
            else return done(null, false, req.flash('loginMessage', 'Login Error'))
        }))
}
