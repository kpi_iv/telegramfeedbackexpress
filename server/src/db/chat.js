'use strict'

import mongoose from 'mongoose'
import { territory } from './dbController';

const dataSchema = mongoose.Schema({})

const Model = mongoose.model('chats', dataSchema)

export default Model


export async function idByEmailPass(email, pass) {
    return Model.findOne({ local: { email: email, pass: pass } })
        .select('_id')
}
export async function idByFBIdToken(id, token) {
    return Model.findOne({ facebook: { id: id, token: token } })
        .select('_id')
}

export async function idByGOOGLEIdToken(id, token) {
    return Model.findOne({ google: { id: id, token: token } })
        .select('_id')
}

export async function byEmail(email) {
    return Model.findOne({ "local.email": email })
}

export async function byFBToken(token) {
    return Model.findOne({ "facebook.token": token })
}

export async function byGOOGLEToken(token) {
    return Model.findOne({ "google.token": token })
}
export async function byId(id) {
    return Model.findOne({ "_id": id })
}

export async function add(data) {
    return Model.collection.insertOne(data)
}

export async function all() {
    return Model.find()
}
