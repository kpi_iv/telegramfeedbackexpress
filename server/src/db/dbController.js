'use strict'

import mongoose from 'mongoose'

import * as chat from './chat'
import * as msg from './msg'

export { chat, msg }

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/web', {
        useMongoClient: true,
        // useNewUrlParser: true
    })
    .catch(e => {
        logger.error('Mongo Error->' + e)
    })

const db = mongoose.connection