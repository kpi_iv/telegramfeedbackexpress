'use strict'

import mongoose from 'mongoose'
import { territory } from './dbController';

const dataSchema = mongoose.Schema({
    chatId: Number
})

const Model = mongoose.model('msgs', dataSchema)

export default Model

export async function byChatId(chatId) {
    return Model.find({ chatId })
}


export async function add(data) {
    return Model.collection.insertOne(data)
}
