import 'babel-polyfill';
require('babel-core/register')

import logger from './models/logger'
import methodOverride from 'method-override'
import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'
import expressSession from 'express-session'
import cookieParser from 'cookie-parser'
import mongo from 'mongodb'
import mongoose from 'mongoose'
import http from 'http'
import helmet from 'helmet'
import fs from 'fs'
import passport from 'passport'
import flash from 'connect-flash'
import cors from 'cors'

import socketIo from 'socket.io'
import SocketIoSession from 'session.socket.io'

import passportRoute from './routes/passport'
import socketRoute from './routes/socketl'
import mainRoute from './routes/mainRoute'

import consolidate from 'consolidate'
import ejs from 'ejs'
import { isLoggedInRessurces } from './routes/mainRoute'

passportRoute(passport)


/* App settings */
const app = express()

app.set('port', (process.env.PORT || 3000))

app.engine('html', require('ejs')
    .renderFile);
app.set('views', path.join(__dirname, 'views'))

app.locals.rmWhitespace = true

const myCookieParser = cookieParser('realsecret')
let sessionStore = new expressSession.MemoryStore()

app.use(myCookieParser)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride())
app.use(expressSession({
    cookie: { maxAge: 3 * 60 * 60 * 1000 }, //3 hours
    store: sessionStore,
    resave: true,
    saveUninitialized: true,
    secret: 'realsecret'
}))
// app.use(cors)

/* Socket connection */
const server = http.Server(app)
const io = socketIo(server)
const sessionSockets = new SocketIoSession(io, sessionStore, myCookieParser)
sessionSockets.on('connection', socketRoute)

/* Connect passport */
app.use(passport.initialize())
app.use(passport.session()) // persistent login sessions

/* Connect flash */
app.use(flash())

/* Connect helmet */
app.use(helmet())
app.disable('x-powered-by')

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})

mainRoute(app, passport)
app.use(express.static(path.join(__dirname, 'public')))
app.use(express.static(__dirname.replace(/server\/.*/, 'client/dist/Lab2'), {
    // maxage: '2h'
}))

app.get('*', (req, res, next) => {
    const newErr = new Error("We can't find the page you're looking for :(")
    newErr.status = 404
    return next(newErr)
})
app.use((err, req, res, next) => {
    err.status = err.status || 500
    res.status(err.status)
    if (err.status == 500) err.message = 'Internal Server'
    res.render('config/error.ejs', { err: err.status, message: err.message, Account: req.session.user })
})

server.listen(app.get('port'), () => {
    logger.debug('Server started vizit here -> http://127.0.0.1:' + app.get('port'))
})
